# Directory Bookmarks for BASH (c) 2009, Ira Chayut, Version 090927
#
# To use, save this file as ~/.bashDirB and add the following line to ~/.bashrc:
#
#        source ~/.bashDirB
#
# DirB and its implementation in this file are the product of, and
# copyrighted by Ira Chayut.  You are granted a non-exclusive, royalty-free
# license to use, reproduce, modify and create derivative works from DirB;
# providing that credit is given to DirB as a source material.
#
# The lastest version is available from: http://www.dirb.info/bashDirB.  Ira can
# be reached at ira@dirb.info.
#
# By default DirB will have the shell echo the current working directory out
# to the title bars of Xterm windows.  To disable this behavior, comment
# out the next line.
# PS1="\[\033]0;\w\007\]\t \!> "

# If the repository of bookmarks does not exist, create it
if  [ ! -e ~/.DirB ]
then
    mkdir ~/.DirB
fi

# "s" - Save bookmark
function s () {
    if [ -n "$2" ]
    then
        # build the bookmark file with the contents "$CD directory_path"
        ( echo '$CD ' \"$2\" > ~/.DirB/"$1" ;) > /dev/null 2>&1
    else
        # build the bookmark file with the contents "$CD directory_path"
        ( echo -n '$CD ' > ~/.DirB/"$1" ;
          pwd | sed "s/ /\\\\ /g" >> ~/.DirB/"$1" ; ) > /dev/null 2>&1

    fi

    # if the bookmark could not be created, print an error message and
    # exit with a failing return code
    if [ $? != 0 ]
    then
        echo bash: DirB: ~/.DirB/"$1" could not be created >&2
        false
    fi
}

# "g" - Go to bookmark
function g () {
    # if no arguments, then just go to the home directory
    if [ -z "$1" ]
    then
        cd
    else
        # if $1 is in ~/.DirB and does not begin with ".", then go to it
        if [ -f ~/.DirB/"$1" -a ${1:0:1} != "." ]
        then
            # update the bookmark's timestamp and then execute it
            touch ~/.DirB/"$1" ;
            CD=cd source ~/.DirB/"$1" ;
        # else just do a "cd" to the argument, usually a directory path of "-"
        else
            cd "$1"
        fi
    fi
}

# "sl" - Saved bookmark Listing
function sl () {
    ls -xt ~/.DirB 
}

# "r" - Remove a saved bookmark
function r () {
        rm ~/.DirB/"$1"
}

# "d" - Display (or Dereference) a saved bookmark
# to use: cd "$(d xxx)"
function show () {
   cat ~/.DirB/$1 | cut -d ' ' -f2
}
